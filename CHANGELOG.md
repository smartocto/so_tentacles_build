# Changelog

## Version 2.4.7.0
* Deselecting anchor and removing highlight on teaser after closing AB test form

## Version 2.4.6.1
### TENTACLES
* Bugfix: Do not load AI suggestions if the tool is not active.

## Version 2.4.6.0
### TENTACLES
* Optimized Cognito login flow

## Version 2.4.5.1
### TENTACLES
* Removed default clickType 'linkClick' when no type is defined. This causes unintended skew growth in case of improperly set tentacles script.

## Version 2.4.5.0
### TENTACLES
* Updating logout procedure if API returns a 401

## Version 2.4.4.0
### TENTACLES
* Implement teaser collection for optimalisation
* Resetting login handling for users with third party login (Cognito) after changes in real-time frontend.
## Smatrocto.ai
* Refactor NorthStar handling and improve suggestion loading logic

## Version 2.4.3.0
### TENTACLES
* Add support for images AVIF format
## Smatrocto.ai
* Display a proper message if an error occurs in the communication to the Smatrocto.ai-API.
* Bugfix: Reset Smatrocto.ai-pane on SPA websites when navigating to another page.

## Version 2.4.2.1
### TENTACLES
* Bugfix: Under certSmatrocto.ain circumstances, images were not replaced during an AB test.

## Version 2.4.2.0
### TENTACLES
* Reduce partyId timeout from two years to 180 days 
* Bugfix: Add background color to Smatrocto.ai suggestions list, for smaller screensizes
* Bugfix: Scroll depth location

## Version 2.4.1.0
### TENTACLES
* Optimised logout procedure for Cognito
* Add ability to define relationId in videoPlay event
* Resolved; retrieving article text when creating alternate headings

## Version 2.4.0.0
### TENTACLES
* Use replacementElements in ab-test configuration to allow users to add own tags that are not supported by default.
* Enhance ABTester button with sparkles and update text handling

## Version 2.3.2.0
### TENTACLES
* Implement functionality for tentacles.autotrack boolean. Datalayer events will still be sent if a datalayer configuration is there.
* Refactor forgot password link to use new URL format

## Version 2.3.1.0
### TENTACLES
* Add preparation for teaser collection in visitor script
* Optimised image and text detection during AB test configuration
* Remove some unused functions to reduce code-basesize

## Version 2.3.0.2
### TENTACLES
* HOTFIX: Video teasers were not recognised during AB test configuration

## Version 2.3.0.1
### TENTACLES
* HOTFIX: While configuring an AB test, titles consisting of two parts were shown together in the first option text field.

## Version 2.3.0.0
### TENTACLES
* Removed requirement for an item present in smartocto for configuring an AB test.
* Aspect ratio caclulation for AB test of images updated
* Selection headers optimised during AB test configuration.

## Version 2.2.2.0
### TENTACLES
* Fix mouseover effect on scroll depth bars
* During an A/B test configuration, other selected texts are prevented from being overwritten.

## Version 2.2.1.0
### TENTACLES
* Manually trigger 'input' event on headline regeration to resize textarea in ABTestPanel
* If no domSmatrocto.ain_searchpath is found in configuration, we will use the body tag position as mSmatrocto.ain position to get the Rect Bounding

## Version 2.2.0.0
### TENTACLES + Smatrocto.ai
* **Enhanced Headline Testing**: Clients with smartocto.Smatrocto.ai can now generate alternative headlines with one click, applicable for tests with or without images.

## Version 2.1.0.2
### TENTACLES
* Fixed problem for AB tests with images. If an image's url contSmatrocto.ained an &, it was converted to &amp; this led to a mismatch during replacement and thus no impressions and clicks.

## Version 2.1.0.1
### TENTACLES
* Fixed an issue with an undeclared variable in the process of adding and removing Anchors to links
* Fixed an issue that caused the anchor is placed at the bottom of the page.

## Version 2.1.0.0
### TENTACLES
* Add support for federated login such as Google or Microsoft
* Updated image test configuration by bypassing CORS errors and support relative paths
* Add support for window.tentacles.test.elements to define elements that could be selected during an AB-test configuration
* Reduce the wSmatrocto.ait time metadata check and send on an article-page
### Smatrocto.ai
* Did some GUI improvements
* Fix bug that displays 'undefined' in readability result

## Version 2.0.3.1
### Smatrocto.ai
* Add support for optimised generated headlines for social and attention time.

## Version 2.0.3.0
### Tentacles
* Update title and picture replacement logic in tentacle-visitor module so that no impressions and clicks are sent if picture or title match.
* Update error messages in tentacle-visitor module for picture and title replacement.
### Smatrocto.ai
* Bugfix: Numbers api-endpoint was called without a proper login resulting in a http 401.

## Version 2.0.2.0
### Tentacles
* Add support for no cookies in tentalces. Please see our knowledgebase for instructions
* Changed security flags in cookies
* Update text styling in for scrolldepth elemement
### Smatrocto.ai
* Add link to story in description field when a notification (suggestion) is clicked 
* Changed a bit the layout of the calendar inside the credits-console

## Version 2.0.1.2
### Tentacles
* Minor bug fix for readingtime on the second+ article pages on an SPA

## Version 2.0.1.1
### Tentacles
* Minor bug fix for recognising videos in a/b testing.

## Version 2.0.1.0
### Tentacles
* Removed hard image ratio alert during image test configuration
* Keep showing scrolldepth and tentacleAnchors even if Tentacles dock is minimized
### Smatrocto.ai
* Add credits dashboard to understand the consumption of credits

## Version 2.0.0.1
* Hotfix numbers endpoint was requested during AB-test configuration...

## Version 2.0.0.0
* Tentacles 2.0 layout is set as default

## Version 1.8.4.1
* HOTFIX: Tentacles dock loading error if there is no mSmatrocto.ain_content defined.

## Version 1.8.4.0
### Tentacles
* Add aspect ratio check when uploading an image for a/b tests
* Layout fixes for Tentacles 2.0
* Set default scroll depth device
### Smatrocto.ai
* Improvements in processing Smatrocto.ai-results
* Hide Tentacles scroll-depth when Smatrocto.ai-overlay is activated
* Feedback is shown when Smatrocto.ai-result is copied to system clipboard

## Version 1.8.3.0
* Improvements of Tentacles layout 2.0
* Add support to manually send pageviews and linkclicks
* Add abillity to change offset of anchors.
* If loyaltyClick data isn't measured, hide element from story-info
* Fix image placeholder event listener bug
* Replace loading animation in Smatrocto.ai-overlay

## Version 1.8.2.0
* Replace browser confirm box for floating window in Tentacles 2.0
* Replace error messages in a/b test configuration panel for floating window
* Check if landing pages are enabled and if non-article page is landing page before sending readingtime events to ingestion
* Add brand related settings to Smatrocto.ai-overlay
* Add information about the numbers in the top section
* Add max number of credits to numbers section of Smatrocto.ai-overlay

## Version 1.8.1.2
* BUGFIX: readingtime events were sending on all pages

## Version 1.8.1.1
* HOTFIX: V1 could not log in anymore

## Version 1.8.1.0
* Made login screen brand depended
* Change front locations from remote to local.

## Version 1.8.0.0
* Add sneak peek of Tentacles 2.0
* BUGFIX: Fixed the bug that doubles filters on an SPA
* BUGFIX: Replace green overlay at image testing for a green border, 
  to prevent misbehaviours if :before or :after virtual classes are added to objects.
* BUGFIX: Requested many of the same clickCVI's after AB-test window is closed.
* BUGFIX: No authentication errors were visual if a login fSmatrocto.ailed.

## Version 1.7.1.0
* Display error messages during creating an AB-test if no original story could be found or test could not be created.

## Version 1.7.0.3
* Add a light background colour to the scroll depth chart so you can see where the chart extends.
* Changed text inside the balloon that is shown when you hover a regular bar on the scrolldepth chart.
* BUGFIX: Fixed bug where device type for scrolldepth is not held.

## Version 1.7.0.2
* BUGFIX: updated impression filtering

## Version 1.7.0.1
* BUGFIX: Made optimalisations to display scrollDepth on SPA-sites

## Version 1.7.0.0
* Add scrollDepth overlay based on mSmatrocto.ainContent position
* Made tooltip dependent if imageTesting is activated or not.
* BUGFIX: When a test is canceled, several times was asked if I am sure I want to cancel
* BUGFIX: Due to a bug, it was not possible to include more than two images in a test.

## Version 1.6.0.8
* Fix the issue that headline tests with picture involved do get less impressions

## Version 1.6.0.7
* Use option A from testData set as the old title instead of the article title, as it can sometimes be different.

## Version 1.6.0.6
* Fix Option C, don't get a text field to type a text for a combination
* Fix deletion of subitems when headertest window is closed while creating a new test
* Remove green haze on hover from photo's that have been involved in a test configuration

## Version 1.6.0.5
* Do not sent headertest data in a linkClick if a title replacement fSmatrocto.ailed.

## Version 1.6.0.4
* Remove 5 minutes message to refresh the page, this feature need to be improved

## Version 1.6.0.3
* BUGFIX: Turned off replacement back for titles, if it's not properly replaced. Need to be improved.

## Version 1.6.0.2
* BUGFIX: Resolving an unauthorised too-early split of content of the teaserPosition

## Version 1.6.0.1
* BUGFIX: Move anchors and Iframe as last elements in document.body

## Version 1.6.0.0
* Add functionality to do headline tests with pictures and headers simultaneous.
* Add functionality to give a warning if the page isn't refreshed for 5 minutes so probably the teasers are changed.
* Tighten restore original title if replacemenent fSmatrocto.ailed. 
* BUGFIX: On sides were darkcode was used, text in teaser anchors were invisible

## Version 1.5.0.0
* Add brand id as optional param for clients to set as part of global scope

## Version 1.4.8.0
* Create documentId for dataLayer events based on SHA256 instead of Base64 to have a max length. This reduces the risk of
  data loss due to the max length of the documentId field in the database.

## Version 1.4.7.4
* BUGFIX: Anchor display for pages without articleId in URL

## Version 1.4.7.3
* BUGFIX: After premature termination of an ab test, sub-textareas were not reset

## Version 1.4.7.2
* BUGFIX: After premature termination of an ab test, new tests did not contSmatrocto.ain correct sub-id's

## Version 1.4.7.1
* BUGFIX: Headertest could not be started due to a missing article title

## Version 1.4.7.0
* Add support for splitted headers
* Add hasVideo parameter to datalayer Events

## Version 1.4.6.1
* Automated CI/CD pipeline for tentacles
* Added channel parameter support for video events

## Version 1.4.5.0 (2023-04-18)
* Changed xpath notation in linkClick and loyaltyClicks events to improve colors in the dashboard.

## Version 1.4.1 (2022-11-21)
* Removed sotm script instance from reading-time.js

## Version 1.2.0 (2022-11-21)
* In an SPA environment, the tentacle.js script is now loaded only once and not on every page change
* Tentacles has started using the more generic ingestion API (ingestion.js), but it still includes support for SOTM
  events. Remotely, settings by smartocto we will ensure that the switch to the Ingestion API is made. Events will then be sent to
  ingestion.smartocto.com.

## Version 1.1.3 (2022-11-21)
* Removed static forge.js and wgxpath.js scripts that are now integrated in the tentacle.js script

